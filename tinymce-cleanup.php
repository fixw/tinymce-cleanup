<?php
/*
Plugin Name: TinyMCE - Copy&Paste Cleanup
Plugin URI: http://felixwerner.com/
Description: Modifications of the TinyMCE Editor. Adds a cleanup function for pasting content from Word/Pages. removes Classes like p1,s1
Author: Felix Werner
Version: 1.0
Author URI: http://felixwerner.com/
*/

/* CODE From: http://jonathannicol.com/blog/2015/02/19/clean-pasted-text-in-wordpress/ */

if ( ! defined( 'ABSPATH' ) ) exit;


add_filter('tiny_mce_before_init','configure_tinymce');

/**
 * Customize TinyMCE's configuration
 *
 * @param   array
 * @return  array
 */
 
function configure_tinymce($in) {
  $in['paste_preprocess'] = "function(plugin, args){
    // Strip all HTML tags except those we have whitelisted
    var whitelist = 'p,span,b,strong,i,em,h3,h4,h5,h6,ul,li,ol';
    var stripped = jQuery('<div>' + args.content + '</div>');
    var els = stripped.find('*').not(whitelist);
    for (var i = els.length - 1; i >= 0; i--) {
      var e = els[i];
      jQuery(e).replaceWith(e.innerHTML);
    }
    // Strip all class and id attributes
    stripped.find('*').removeAttr('id').removeAttr('class');
    // Return the clean HTML
    args.content = stripped.html();
  }";
  return $in;
}


?>